document.getElementById('new_game').addEventListener('click', function() {

    document.getElementById('loadingAxios').style.display = 'block';

    axios.get("newGame").then(function(response) {

        document.getElementById('new_game').style.display = 'none';
        document.getElementById('play-pokemon-now').innerHTML = response.data;
        document.getElementById('continue_game').style.display = 'none';

        var script = document.createElement('script');
        script.src = '/static/js/endGame.js';
        script.type = 'text/javascript';
        document.body.appendChild(script);

        var script = document.createElement('script');
        script.src = '/static/js/choosePokemon.js';
        script.type = 'text/javascript';
        document.body.appendChild(script);

    }).catch(function(error) {
        console.error('Error al cargar el nuevo juego:', error);
    }).finally(function() {
        document.getElementById('loadingAxios').style.display = 'none';
    });
});