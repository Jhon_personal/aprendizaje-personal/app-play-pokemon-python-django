function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

document.getElementById('end_game').addEventListener('click', function() {

    const csrftoken = getCookie('csrftoken');
    var labelPoints = document.getElementById("Puntaje");
    var points = parseInt(labelPoints.textContent);
    console.log(points)
    axios.post("endGame",{
        points
        },{
        headers: {
            'X-CSRFToken': csrftoken
        }
    }).then(function(response) {
        document.getElementById('play-pokemon-now').innerHTML = '';
        document.getElementById('loadingAxios').style.display = 'block';
    }).catch(function(error) {
        console.error('Error ', error);
    }).finally(function() {
        location.reload();
    });
});