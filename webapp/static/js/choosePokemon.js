var pokemonList = document.getElementById('pokemon-list');

pokemonList.addEventListener('click', function(event) {
    if (event.target && event.target.nodeName === "LI") {

        document.getElementById('pokemon-list').style.display = 'none';

        var labelPoints = document.getElementById("Puntaje");
        var points = parseInt(labelPoints.textContent);

        var inputIdHidden = document.getElementById("secretPokemon");
        var inputNameHidden = document.getElementById("secretNamePokemon");

        var idPokemon = inputIdHidden.value;
        var idPokemon = atob(idPokemon);

        var namePokemon = inputNameHidden.value;
        var namePokemon = atob(namePokemon);

        var idChoose = event.target.id;

        if(idPokemon === idChoose){

            document.getElementById('textAnswer').innerHTML = 'Correcto, es '+namePokemon;
            points += 1;
            document.getElementById('Puntaje').innerHTML = points;
            document.getElementById('continue_game').style.display = 'block';
        }else{
            document.getElementById('textAnswer').innerHTML = 'Error, era '+namePokemon;
            document.getElementById('continue_game').style.display = 'none';
        }
    }
});

document.getElementById('continue_game').addEventListener('click', function() {

    var labelPoints = document.getElementById("Puntaje");
    var points = parseInt(labelPoints.textContent);

    document.getElementById('play-pokemon-now').innerHTML = '';
    document.getElementById('loadingAxios').style.display = 'block';

    axios.get("newGame").then(function(response) {

        document.getElementById('new_game').style.display = 'none';
        document.getElementById('play-pokemon-now').innerHTML = response.data;

        document.getElementById('Puntaje').innerHTML = points;

        var script = document.createElement('script');
        script.src = '/static/js/endGame.js';
        script.type = 'text/javascript';
        document.body.appendChild(script);

        var script = document.createElement('script');
        script.src = '/static/js/choosePokemon.js';
        script.type = 'text/javascript';
        document.body.appendChild(script);

    }).catch(function(error) {
        console.error('Error al cargar el nuevo juego:', error);
    }).finally(function() {
        document.getElementById('loadingAxios').style.display = 'none';
    });
});
