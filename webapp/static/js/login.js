
    var modal = document.getElementById('modal');
    var modalContent = document.getElementById('modalContent');

    // Mostrar el modal automáticamente
    modal.style.display = 'block';

    // Cerrar modal al hacer clic en la "X" o fuera del modal
    var closeButton = document.getElementsByClassName('close')[0];
    closeButton.onclick = function() {
        modal.style.display = 'none';
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    }
