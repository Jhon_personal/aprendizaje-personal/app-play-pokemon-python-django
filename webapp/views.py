import base64
import json
import random
from datetime import datetime
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from .functions import Services

# Create your views here.
def login(request):
    return render(request, "login.html")

def register(request):
    return render(request, "register.html")

def recovery(request):
    return render(request, "recovery.html")

def loginForm(request):

    if request.method == 'POST':
        datos_formulario = request.POST

        data_response = Services._login(datos_formulario['usuario'], datos_formulario['pass'])

        code_response = data_response['code']
        msg_response = data_response['message']
        dataUser = data_response['dataUser']


        if code_response == '200':

            data_ranking = rankingForm(dataUser['usuario'])
            data_ranking_user = rankingUserForm(dataUser['usuario'])
            ranking = list(data_ranking_user['dataUser'].keys())[0]
            puntos = data_ranking_user['dataUser'][ranking]['puntos']

            data_ranking_user = {'ranking': ranking, 'puntos': puntos}


            html_ranking = render_to_string('ranking.html', data_ranking)
            html_game = render_to_string('game.html')
            html_data = render_to_string('data.html', data_ranking_user)

            mensajes = {'msg_response': msg_response, 'user_name': dataUser['usuario'], 'ranking': html_ranking,
                        'game': html_game, 'data': html_data}

            request.session['emailUser'] = dataUser['correo']

            return render(request, "home.html", mensajes )
        else:

            mensajes = {'msg_response': msg_response}
            return render(request, "login.html", mensajes)
    else:
        return render(request, "login.html")

def registerForm(request):

    datos_formulario = request.POST

    data_response = Services._register(datos_formulario['correo'], datos_formulario['usuario'], datos_formulario['pass'])

    code_response = data_response['code']
    msg_response = data_response['message']
    dataUser = data_response['dataUser']

    if code_response == '200':

        data_ranking = rankingForm(dataUser['usuario'])
        data_ranking_user = rankingUserForm(dataUser['usuario'])
        ranking = list(data_ranking_user['dataUser'].keys())[0]
        puntos = data_ranking_user['dataUser'][ranking]['puntos']

        data_ranking_user = {'ranking': ranking, 'puntos': puntos}

        html_ranking = render_to_string('ranking.html', data_ranking)
        html_game = render_to_string('game.html')
        html_data = render_to_string('data.html', data_ranking_user)

        mensajes = {'msg_response': msg_response, 'user_name': dataUser['usuario'], 'ranking': html_ranking,
                    'game': html_game, 'data': html_data}

        request.session['emailUser'] = dataUser['correo']

        return render(request, "home.html", mensajes)
    else:

        mensajes = {'msg_response': msg_response}
        return render(request, "login.html", mensajes)


def recoveryForm(request):

    datos_formulario = request.POST

    data_response = Services._recoveryPass(datos_formulario['correo'])

    code_response = data_response['code']
    msg_response = data_response['message']
    dataUser = data_response['dataUser']

    if code_response == '200':

        mensajes = {'msg_response': msg_response, 'pass': dataUser['pass'], 'correo': datos_formulario['correo']}

        return render(request, "recovery.html", mensajes)
    else:

        mensajes = {'msg_response': msg_response}

        return render(request, "login.html", mensajes)


def rankingForm(usuario):

    data_response = Services._ranking(usuario)

    return data_response

def rankingUserForm(usuario):

    data_response = Services._rankingUser(usuario)

    return data_response

def newGame(request):

    data_response = Services._pokemonInfo()

    img_poke = data_response['sprites']['other']['official-artwork']['front_default']
    id_poke = data_response['id']
    name_poke = data_response['name']
    pokemon_ok = {'id': id_poke, 'name': name_poke}

    data_response_random = Services._pokemonRandom()
    data_response_random.append(pokemon_ok)
    random.shuffle(data_response_random)

    id_codificado = base64.b64encode(str(id_poke).encode()).decode()
    name_codificado = base64.b64encode(str(name_poke).encode()).decode()

    mensajes = {'img_pokemon': img_poke, 'id_pokemon': id_codificado, 'name_pokemon': name_codificado, 'optionsPokemon': data_response_random}

    return render(request, "newGame.html", mensajes)


def endGame(request):

    if request.method == 'POST':

        data  = json.loads(request.body)
        points = data.get('points')
        emailUser = request.session.get('emailUser')

        fecha_actual = datetime.now().strftime('%Y-%m-%d')

        data_response = Services._savePoints(emailUser, points, fecha_actual)

        return JsonResponse(data_response)